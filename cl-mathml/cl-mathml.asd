;;; -*- Mode: Lisp -*-

;;; cl-mathml.system

(defparameter *_cl-xml-load-file*
  (pathname
   ;; "/Users/marcoxa/Projects/Lang/CL/cl-xml/code/load.lisp"
   "z:/lang/cl/cl-xml/code/load.lisp"
   ;; "z:/lisp/CL-XML/code/load.lisp"
   ))


(eval-when (:load-toplevel :execute)
  (unless (find-package "XML-PARSER")
    (loop while (not (and *_cl-xml-load-file* (probe-file *_cl-xml-load-file*))) do
          (cerror "Enter the pathanme of CL-XML load file. Use either a string or a pathname."
                  "Cannot find the ~S file.~@
                   CL-XML is not available and therefore the system cannot~@
                   be setup."
                  *_cl-xml-load-file*)
          (format t "> ")
          (setf *_cl-xml-load-file*
                (ignore-errors (pathname (read))))
          (fresh-line)
          )
    (load *_cl-xml-load-file*)))


(asdf:defsystem "CL-MATHML"
    :components ("cl-mathml-package"
		 "math"
		 "mathml-parser"
		 "mathml-writer"
		 ))


(eval-when (:load-toplevel :execute)
  (format t "Setting the CL-MATHML LP Translation.~%")
  (setf (logical-pathname-translations "CL-MATHML")
        `(("*.*" ,(make-pathname :name :wild
                                 :type :wild
                                 :defaults *load-pathname*))
          ("**;*.*" ,(make-pathname :name :wild
                                    :type :wild
                                    :directory (append (pathname-directory *load-pathname*)
                                                       (list :wild-inferiors))
                                    :defaults *load-pathname*))
          )))


;;; end of file -- cl-mathml.system --
