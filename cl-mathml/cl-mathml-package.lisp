;;; -*- Mode: Lisp -*-

(defpackage "CL-MATHML" (:use "COMMON-LISP")
  ;; Interface.
  (:export "BUILD-FORM-FROM-MATHML")

  ;; Other exported symbols.
  (:export
   "PIECEWISE"
   "PIECE"
   "OTHERWISE")

  (:export
   "CI"
   "CN"
   "CSYMBOL")
  )

;;; end of file -- mathml-package.lisp --
