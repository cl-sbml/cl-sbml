;;; -*- Mode: Lisp -*-

;;; mathml-writer.lisp --
;;; This code takes care of the MatML subset used in the SBML specification.
;;; However, it could be extended to take care of the full MathML specification.

(in-package "CL-MATHML")


;;; Protocol.

(defgeneric mathml-writer (math-expr stream &key &allow-other-keys)
  (:documentation
   "The main function used to dump a math S-expression to a STREAM."))




;;;===========================================================================
;;; Implementation.


;;;---------------------------------------------------------------------------
;;; Variables and constants.

;;; (defvar *mathml-url* "http://www.w3.org/1998/Math.MathML")


;;;---------------------------------------------------------------------------
;;; Base methods and utility functions.

(defmethod mathml-writer ((m math) (s stream) &key &allow-other-keys)
  (format s "<math>~%")
  (pprint-logical-block (s (math-form m))
    (pprint-indent :block 4)
    (format s "~S~%" (math-form m)))
  (format s "</math>~%"))



;;; end of file -- mathml-writer.lisp --
