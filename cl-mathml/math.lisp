;;; -*- Mode: Lisp -*-

(in-package "CL-MATHML")

(defstruct (math (:print-object print-math))
  (form nil :type t)
  )


(defun print-math (m stream &optional (print-depth 0))
  (declare (ignore print-depth)
           (type stream stream))
  (print-unreadable-object (m stream :identity t :type t)
    (format stream "~S" (math-form m))))

;;; end of file -- math.lisp --
