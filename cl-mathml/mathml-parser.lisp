;;; -*- Mode: Lisp -*-

;;; mathml-parser.lisp --
;;; This code takes care of the MatML subset used in the SBML specification.
;;; However, it could be extended to take care of the full MathML specification.

(in-package "CL-MATHML")


;;; Protocol.

(defgeneric build-form-from-mathml (item &key &allow-other-keys)
  (:documentation
   "The main function used to parse a MathML ITEM.
The result is a Common Lisp S-expression which may be directly
evaluated, give a proper environment."))


(defgeneric mathml-parser (node &key &allow-other-keys))


(define-condition mathml-parse-error (parse-error)
  ((node :accessor mathml-parse-error-node
         :initarg :node
         :initform nil)
   (format-control :accessor mathml-parse-error-format-control
                   :initform ""
                   :initarg :format-control)
   (format-arguments :accessor mathml-parse-error-format-arguments
                     :initform ()
                     :initarg :format-arguments)
   )
  (:report (lambda (c s)
             (format s "CL-MATHML Parse Error for ~S: ~?"
                     (mathml-parse-error-node c)
                     (mathml-parse-error-format-control c)
                     (mathml-parse-error-format-arguments c)))))


;;;===========================================================================
;;; Implementation.


;;;---------------------------------------------------------------------------
;;; Variables and constants.

(defvar *mathml-url* "http://www.w3.org/1998/Math.MathML")


;;;---------------------------------------------------------------------------
;;; Base methods and utility functions.

(defun node-arguments (apply-node)
  (cl-xml-ext:filter-away-strings (xqdm:children apply-node)))


(defmethod build-form-from-mathml ((m xqdm:elem-node) &key &allow-other-keys)
  (with-accessors ((name xqdm:name))
      m
    (unless (string-equal name "math")
      (error 'mathml-parse-error
             :node m
             :format-control "node name is ~S, it should be \"math\"."
             :format-arguments (list name)))
    (mathml-parser m)))



(defmethod mathml-parser ((m xqdm:elem-node) &key &allow-other-keys)
  (with-accessors ((name xqdm:name))
      m
    (cond ((string-equal name "math")
           (let ((apply-subnode (cl-xml-ext:get-element-component m "apply"))
                 (piecewise-subnode (cl-xml-ext:get-element-component m "piecewise"))
                 )
             (cond (apply-subnode
                    (make-math :form (mathml-parser apply-subnode)))
                   (piecewise-subnode
                    (make-math :form (mathml-parser piecewise-subnode)))
                   (t
                    (make-math :form nil)))))
          
          ((string-equal name "apply")
           (let ((args (node-arguments m)))
             (cons (translate-apply-operator (first args))
                   (mathml-parser (rest args)))))

          ((string-equal name "piecewise")
           (let ((args (node-arguments m)))
             (cons 'piecewise
                   (mathml-parser (rest args)))))

          ((string-equal name "piece")
           (let ((args (node-arguments m)))
             (cons 'piece
                   (mathml-parser (rest args)))))

          ((string-equal name "otherwise")
           (let ((args (node-arguments m)))
             (cons 'piece
                   (mathml-parser (rest args)))))
          
          ((string-equal name "ci")
           (cons 'ci (first (xqdm:children m))))

          ((string-equal name "cn")
           (cons 'cn (read-from-string (first (xqdm:children m)))))

          ((string-equal name "csymbol")
           (cons 'csymbol (first (xqdm:children m))))

          (t (list m))
          )))


(defmethod mathml-parser ((m list) &key &allow-other-keys)
  (when m
    (cons (mathml-parser (first m))
          (mathml-parser (rest m)))))



(defun translate-apply-operator (op)
  (declare (type xqdm:elem-node op))
  (with-accessors ((name xqdm:name))
      op
    (cond ((string-equal name "eq") '=)
          ((string-equal name "neq") '/=)
          ((string-equal name "gt") '>)
          ((string-equal name "lt") '<)
          ((string-equal name "geq") '>=)
          ((string-equal name "leq") '<=)

          ((string-equal name "plus") '+)
          ((string-equal name "minus") '-)
          ((string-equal name "times") '*)
          ((string-equal name "divide") '/)

          ((string-equal name "power") 'expt)
          ((string-equal name "root") 'root) ; Not in CL package!
          ((string-equal name "abs") 'abs) ; Maybe need to check types.
          ((string-equal name "exp") 'expt)
          ((string-equal name "ln")
           (warn "MathML parsing: rendering \"ln\" as \"log\": This is incorrect.~%")
           'log)
          ((string-equal name "log") 'log)
          ((string-equal name "floor") 'floor)
          ((string-equal name "ceiling") 'ceiling)

          ((string-equal name "factorial") 'factorial) ; Not in CL package!

         
          ((string-equal name "and") 'and)
          ((string-equal name "or") 'or)
          ((string-equal name "xor") 'xor) ; Not in CL package!
          ((string-equal name "not") 'not)

          ((string-equal name "sin") 'sin)
          ((string-equal name "cos") 'cos)
          ((string-equal name "tan") 'tan)
          ((string-equal name "sec") 'sec)
          
          #| Other missing |#

          (t (warn "MathML Parsing: ~S operator unknown.  Returning as is.~%")
             name)

          )))

;;; end of file -- mathml-parser.lisp --
