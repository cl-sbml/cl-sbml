(in-package "CL-SBML")

(defstruct (compartment (:include sbml-element))
  "The Compartment Structure Class.
The representation of compartments in SBML."
  (spatial-dimensions 3 :type (member 0 1 2 3))
  (size 0.0d0 :type double-float)
  (units nil :type (or symbol string cons)) ; Actually the STRING should be a SID.
  (outside nil :type (or null string))      ; Actually the STRING should be a SID.
  (constant-p t :type boolean)
  )

;;; Check the Compartment_initDefaults function in libSbml.

;;; end of file -- compartment.lisp --
