(in-package "CL-SBML")

(defstruct (sbml (:include sbml-base) (:constructor make-sbml (&key model)))
  "The SBML Structure Class.
The SBML structure represents the top-level elements in SBML
documents."
  (level 2 :type (member 2) :read-only t)
  (version 1 :type (member 1) :read-only t)
  (model nil :type (or null model))
  )

;;; end of file -- sbml.lisp --
