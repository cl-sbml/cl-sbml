(in-package "CL-SBML")

(defstruct (algebraic-rule (:include rule))
  "The SBML Algebraic Rule Structure Class."
  )

;;; end of file -- algebraic-rule.lisp --
