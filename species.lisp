(in-package "CL-SBML")

(defstruct (species (:include sbml-element))
  (compartment nil :type (or null string compartment)) ; Actually the STRING should be a SID.
  (initial-amount 0.0d0 :type double-float)
  (initial-concentration 0.0d0 :type double-float)

  ;; substance-units, spatial-units:
  ;; Actually the STRING should be a SID. Moreover, we allow more
  ;; complex unit definitions than what admitted in SBML, as we are
  ;; assuming already "preprocessed" data structures. 
  (substance-units nil :type (or null string unit list))
  (spatial-units nil :type (or null string unit list))

  (has-only-substance-units-p nil :type boolean)

  (boundary-condition nil :type boolean)
  (charge 0 :type fixnum)
  (constant-p nil :type boolean)
  )

;;; end of file -- species.lisp --
