(in-package "CL-SBML")

(eval-when (:load-toplevel :compile-toplevel :execute)
  (defconstant +unit-kinds+
    '(
      :AMPERE
      :BECQUEREL
      :CANDELA
      :CELSIUS
      :COULOMB
      :DIMENSIONLESS
      :FARAD
      :GRAM
      :GRAY
      :HENRY
      :HERTZ
      :ITEM
      :JOULE
      :KATAL
      :KELVIN
      :KILOGRAM
      :LITER
      :LITRE
      :LUMEN
      :LUX
      :METER
      :METRE
      :MOLE
      :NEWTON
      :OHM
      :PASCAL
      :RADIAN
      :SECOND
      :SIEMENS
      :SIEVERT
      :STERADIAN
      :TESLA
      :VOLT
      :WATT
      :WEBER
      :INVALID)
    ))


(deftype unit-kind ()
  `(member ,+unit-kinds+))


(declaim (inline unit-kind-p))

(defun unit-kind-p (x)
  (typep x 'unit-kind))

;;; end of file -- unit-kind.lisp --
