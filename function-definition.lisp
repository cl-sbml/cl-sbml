(in-package "CL-SBML")

(defstruct (function-definition (:include sbml-element))
  "The SBML Function Definition Structure Class.
Associates an identifier to a MathML element containing a Lambda
expression. The identifier can then be used in other SBML elements."
  (math () :type (or null cons))
  )

;;; end of file -- function-definition.lisp --
