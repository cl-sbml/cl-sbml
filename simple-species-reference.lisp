(in-package "CL-SBML")

(defstruct (simple-species-reference (:include sbml-base))
  "The SBML Simple Species Reference Structure Class."
  (species nil :type (or symbol string))
  )

;;; end of file -- simple-species-reference.lisp --
