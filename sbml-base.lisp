(in-package "CL-SBML")

(defstruct (sbml-base (:constructor nil))
  "The SBML-BASE Structure Class.
The SBML-BASE structure represents the top level conceptual node in
the SBML hierarchy.
Most CL-SBML elements inherit it."
  (meta-id nil :type (or null string))
  (annotation nil :type (or null string xqdm:elem-node))
  (notes nil :type (or null string xqdm:elem-node))
  )

;;; end of file -- sbml-base.lisp --
