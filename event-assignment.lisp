(in-package "CL-SBML")

(defstruct (event-assignment (:include sbml-base))
  "The SBML Event Assignment Structure Class."
  (variable nil :type (or symbol string)) ; Actually a SID.
  (math () :type (or null cons t)) ; To accommodate MathML XQDM:NODE.
  )

;;; end of file -- event-assignment.lisp --
