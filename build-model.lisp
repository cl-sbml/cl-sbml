;;;; -*- Mode: Emacs -*-

(in-package "CL-SBML")


;;;===========================================================================
;;; System dependencies.

;;; LW needs the following to make sure that the INFIX parser works
;;; correctly.  The setting makes LW extend the stack group size
;;; silently.  If not set to NIL a continuable error is signalled, which
;;; confuses the INFIX package input stream.  This means that the
;;; INFIX package input stream handling is buggy, but there's no time
;;; to fix it.  Morevoer, there is the need of a work-around in
;;; Windows.

#+(and lispworks mac)
(eval-when (:load-toplevel :execute)
  (setf sys:*stack-overflow-behaviour* nil))

#+(and lispworks win32)
(eval-when (:load-toplevel :execute)
  (warn "Automatic and silent stack group extension not implemented ~@
         for LWW.  INFIX may not work properly.")
  (setf sys::*stack-overflow-behaviour* nil))




;;;===========================================================================
;;; Protocol.

(defgeneric build-model-from-sbml (source)
  (:documentation
   "The main constructor for the internal SBML model representation.
It takes a SOURCE (which can be a PATHNAME, a STRING, or a parsed SBML
document and returns  SBML:MODEL Common Lisp structure. The parsed
document is also returned as a second value."))


(declaim (inline true-string-p false-string-p))

(defun true-string-p (s)
  (declare (type (or string symbol) s))
  (string-equal "true" s))

(defun false-string-p (s)
  (declare (type (or string symbol) s))
  (string-equal "false" s))




(defmethod build-model-from-sbml ((filename pathname))
  (build-model-from-sbml (load-sbml filename)))


(defmethod build-model-from-sbml ((sbml-string string))
  (build-model-from-sbml (load-sbml sbml-string)))


(defmethod build-model-from-sbml ((sbml-stream stream))
  (build-model-from-sbml (load-sbml sbml-stream)))


(defmethod build-model-from-sbml ((document xqdm:doc-node))
  (let ((model (get-model document)))
    (values
     (make-model :id (get-model-id model)
                 :name (get-model-name model)
                 :notes (get-model-notes model)
                 :annotation (get-model-annotation model)
                 :compartments (collect-compartments
                                (get-model-compartments model))
                 :species (collect-species (get-model-species model))
                 :parameters (collect-parameters (get-model-parameters model))
                 :rules (collect-rules (get-model-rules model))
                 :reactions (collect-reactions (get-model-reactions model))
                 :events (collect-events (get-model-events model))
                 )
     document)))



(defun collect-species (species)
  (loop for specie in species
        collect (make-species
                 :name (get-species-name specie)
                 :id (get-species-id specie)
                 :compartment (get-species-compartment specie)
                 :boundary-condition (true-string-p
                                      (get-species-boundary-condition specie))
                 :charge  (get-species-charge specie))))


(defun collect-func-def (fd)
  (loop for f in fd
        collect (make-function-definition
                 :id (get-function-definition-id f)
                 :name (get-function-definition-name f)
                 :math (get-function-definition-math f))))
             

(defun collect-unit-def (ud)
  (loop for u in ud
        collect (make-unit-definition
                 :id (get-unit-definition-id u)
                 :name (get-unit-definition-name u)
                 :units (get-unit-definition-units u))))


(defun collect-compartments (compartments)
  (loop for c in compartments
        collect (make-compartment
                 :id (get-compartment-id c)
                 :name (get-compartment-name c)
                 :spatial-dimensions (get-compartment-spatial-dimensions c)
                 :size (get-compartment-size c)
                 :units (get-compartment-units c)
                 :outside (true-string-p (get-compartment-outside c))
                 :constant-p (get-compartment-constant c))))


(defun collect-parameters (parameters)
  (loop for p in parameters
        collect (make-parameter
                 :id (get-parameter-id p)
                 :name (get-parameter-name p)
                 :value (read-from-string (get-parameter-value p))
                 :units (get-parameter-units p)
                 :constant-p (get-parameter-constant p))))


(defun collect-rules (rules)
  (loop for r in rules
        collect (make-rule 
                 :id (get-rule-id r)
                 :name (get-rule-name r)
                 :formula (get-parameter-formula r)
                 :math (get-parameter-math r))))


(defun collect-algebraic-rules (rules)
  (loop for r in rules
        collect (make-algebraic-rule 
                 :id (get-algebraic-rule-id r)
                 :name (get-algebraic-rule-name r))))

(defun collect-assignment-rules (rules)
  (loop for r in rules
        collect (make-assignment-rule 
                 :id (get-assignment-rule-id r)
                 :name (get-assignment-rule-name r)
                 :variable (get-assignment-rule-variable r))))

(defun collect-rate-rules (rules)
 (loop for r in rules
        collect (make-rate-rule 
                 :id (get-rate-rule-id r)
                 :name (get-rate-rule-name r)
                 :variable (get-rate-rule-variable r))))


(defun collect-reactions (rxns)
  (loop for r in rxns
        collect (make-reaction
                 :name (get-reaction-name r)
                 :reactants (collect-reaction-reactants r)
                 :products (collect-reaction-products r)
                 :modifiers (get-reaction-modifier-list r)
                 :kinetic-law (collect-reaction-kinetic-law r)
                 :reversible-p (true-string-p (get-reaction-reversible r))
                 :fast-p (true-string-p (get-reaction-fast r)))))


(defun collect-events (events)
  (loop for e in events
        collect (make-event 
                 :id (get-event-id e)
                 :name (get-event-name e)
                 :trigger (get-event-trigger e)
                 :delay (get-event-delay e)
                 :time-units (get-event-time-units e)
                 :event-assignments (get-event-event-assignments e))))


(defun collect-specie-references (reaction from-list)
  (loop for reactant in (get-element-components-in-list reaction
                                                        from-list)

        if (string-equal (xqdm:name reactant) "specieReference")
          collect (make-species-reference
                   :species (cl-xml-ext:get-attr-named reactant "specie")
                   )
        else
          collect (make-simple-species-reference
                   :species (cl-xml-ext:get-attr-named reactant "specie")
                   )
        ))


(defun collect-reaction-reactants (reaction)
  (collect-specie-references reaction "listOfReactants"))


(defun collect-reaction-products (reaction)
  (collect-specie-references reaction "listOfProducts"))


(defun collect-reaction-kinetic-law (reaction)
  (let ((kl (get-reaction-kinetic-law reaction)))
    (make-kinetic-law :formula (get-kinetic-law-formula kl)
                      #+infix
                      :parsed-formula
                      #+infix
                      (infix:string->prefix (get-kinetic-law-formula kl))

                      :parameters (collect-parameter-list 
                                   (get-kinetic-law-parameters kl))
                      :math (collect-math (get-kinetic-law-math kl)))))
     
(defun collect-parameter-list (pl)
  (loop for p in pl 
        collect (make-parameter
                 :id (get-parameter-id p)
                 :name (get-parameter-name p)
                 :value (get-parameter-value p)
                 :units (get-parameter-units p)
                 :constant-p (true-string-p (get-parameter-constant p)))))


(defun collect-math (m)
  (and m
       (cl-mathml:build-form-from-mathml m)))


;;; end of file --
