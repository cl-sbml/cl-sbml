;;; -*- Mode: Lisp -*-

(in-package "CL-XML.EXTENSIONS")

;;; The following is an utility function that can be used to access
;;; the "listOf____" elements in the various XML (e.g. SBML) definitions.
;;;
;;; It looks like something along these lines should be in CL-XML.
;;; But I couldn't find it.

(defun get-element-components-in-list (elem list-name)
  (declare (type xqdm:elem-node elem-node))
  (check-type elem xqdm:elem-node)
  (let ((components-list-node
         (find list-name
               (filter-away-strings (xqdm:children elem))
               :key #'xqdm:name
               :test #'string-equal))
        )
    (when components-list-node
      (filter-away-strings
       (xqdm:children components-list-node)))))


(defun get-element-component (elem component-name)
  (declare (type xqdm:elem-node elem-node))
  (check-type elem xqdm:elem-node)
  (find component-name
        (filter-away-strings (xqdm:children elem))
        :key #'xqdm:name
        :test #'string-equal))


(defun get-element-components (elem component-name)
  (declare (type xqdm:elem-node elem-node))
  (check-type elem xqdm:elem-node)
  (remove-if (lambda (e)
               (not (string-equal component-name e)))
             (filter-away-strings (xqdm:children elem))
             :key #'xqdm:name))


(defun get-attr-named (elem-node attr-name)
  (declare (type xqdm:elem-node elem-node))
  (check-type elem-node xqdm:elem-node)
  (let ((attrs (xqdm:attributes elem-node)))
    (when attrs
      (let ((attr (find attr-name (xqdm:attributes elem-node)
                        :key #'xqdm:name
                        :test #'string-equal))
            )
        (when attr
          (or (xqdm:value attr)
              (first (xqdm:children attr)))))
      )))





(defun filter-away-strings (list)
  (remove-if (lambda (x)
               (and (stringp x)
                    (every (lambda (c)
                             (member c '(#\Space
                                         #\Tab
                                         #\Newline)
                                     :test #'char=))
                           x)))
             list))

;;; end of file -- cl-xml-ext.lisp --
