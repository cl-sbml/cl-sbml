;;; -*- Mode: Lisp -*-


(eval-when (:load-toplevel :execute)
  (unless (find-package "XML-PARSER")
    (error "CL-XML.EXTENSION error: the CL-XML library is not available, ~
            therefore it cannot be extended.")))

(asdf:defsystem "CL-XML-EXT"
    :components ("cl-xml-ext-package"
		 "cl-xml-ext"))

;;; end of file -- cl-xml-ext.asdf --
