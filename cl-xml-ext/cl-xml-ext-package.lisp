;;; -*- Mode: Lisp -*-

(defpackage "CL-XML.EXTENSIONS" (:use "COMMON-LISP")
  (:nicknames "CL-XML-EXT")
  (:export
   "GET-ELEMENT-COMPONENT"
   "GET-ELEMENT-COMPONENTS"
   "GET-ATTR-NAMED"

   "GET-ELEMENT-COMPONENTS-IN-LIST"
   
   "FILTER-AWAY-STRINGS"
   ))

;;; end of file -- cl-xml-ext.lisp --
