(in-package "CL-SBML")

(defstruct (model (:include sbml-element)
                  (:print-object print-model))
  "The Model Structure Class.
The main element type in a SBML document.  Each model groups all the
defining characteristics of a given biochemical system."
  (function-definitions () :type list)
  (unit-definitions () :type list)
  (compartments () :type list)
  (species () :type list)
  (parameters () :type list)
  (rules () :type list)
  (reactions () :type list)
  (events () :type list)
  )


(defun print-model (m stream &optional (print-depth 0))
  (declare (ignore print-depth)
           (type stream stream))
  (print-unreadable-object (m stream :identity t :type t)
    (format stream "~:[~;~:*~A ~](~A) ~D species, ~D compartments, ~D reactions"
            (model-name m)
            (model-id m)
            (length (model-species m))
            (length (model-compartments m))
            (length (model-reactions m)))))

;;; end of file -- model.lisp --
