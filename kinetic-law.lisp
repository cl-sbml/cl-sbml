(in-package "CL-SBML")

(defstruct (kinetic-law (:include sbml-base))
  "The Kinetic Law Structure Class.
The instances of this structure class are the representation of SBML
kinetic laws. As an added bonus, CL-SBML always tries to `parse' the
kinetic law and to store its canonical S-expression representation in
the PARSED-FORMULA slot.  The parsing attempt is done for either the
regular textual formula representation stored in the FORMULA slot, or
for the MathML formula rpresentation stored in the MATH slot ."
  (formula "" :type string)
  (parsed-formula nil :type (or null cons))
  (math () :type (or null cons))
  (parameters () :type list)
  (time-units nil :type (or symbol string))
  (substance-units nil :type (or symbol string))
  )

;;; end of file -- kinetic-law.lisp --
