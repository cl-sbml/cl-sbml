(in-package "CL-SBML")

(defstruct (rate-rule (:include rule))
  "The SBML Rate Rule Structure Class."
  (variable nil :type (or string symbol))
  )

;;; end of file -- rate-rule.lisp --
