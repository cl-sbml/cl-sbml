(in-package "CL-SBML")

(defstruct (species-reference (:include simple-species-reference))
  "The SBML Species Reference Structure Class."
  (stoichiometry 0.0d0 :type double-float)
  (stoichiometry-math () :type (or null cons stoichiometry-math))
  )

;;; end of file -- species-reference.lisp --
