(in-package "CL-SBML")

(defstruct (unit-definition (:include sbml-element))
  (units () :type list)
  )

;;; end of file -- unit-definition.lisp --
