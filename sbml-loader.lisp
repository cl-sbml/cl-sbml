;;; -*- Mode: Lisp -*-

;;; sbml-loader.lisp

(in-package "CL-SBML")

(use-package "CL-XML-EXT")

(defgeneric load-sbml (source &key &allow-other-keys)
  (:documentation
   "The main entry point on the CL-SBML loader machiner.
The LOAD-SBML generic function takes a document source and returns a
parsed representation of the SBML file."))



(defmethod load-sbml ((source pathname) &key &allow-other-keys)
  (xmlp:document-parser source))


(defmethod load-sbml ((source string) &key &allow-other-keys)
  (xmlp:document-parser source))


(defmethod load-sbml ((source stream) &key &allow-other-keys)
  (xmlp:document-parser source))




(defmethod get-sbml ((sbml-document xqdm:doc-node))
  (xqdm:root sbml-document))


(defmethod get-model ((sbml-document xqdm:doc-node))
  (find-if (lambda (e) (typep e 'xqdm:elem-node))
           (xqdm:children (xqdm:root sbml-document))))


(defmethod get-model-components ((model xqdm:elem-node))
  (filter-away-strings (xqdm:children model)))



;;; SBML Element accessors.

(defmethod get-sbml-element-id ((se xqdm:elem-node))
  (get-attr-named se "id"))

(defmethod get-sbml-element-name ((se xqdm:elem-node))
  (get-attr-named se "name"))


(defmethod get-sbml-element-notes ((se xqdm:elem-node))
  (get-element-components se "notes"))

(defmethod get-sbml-element-annotations ((se xqdm:elem-node))
  (get-element-components se "annotation"))


;;; Model node accessors.

(defmethod get-model-id ((model xqdm:elem-node))
  (get-sbml-element-id model))

(defmethod get-model-name ((model xqdm:elem-node))
  (get-sbml-element-name model))

(defmethod get-model-notes ((model xqdm:elem-node))
  (let ((notes (get-sbml-element-notes model)))
    (when (> (length notes) 1)
      (warn "CL-SBML: model ~A has more than 1 \"notes\" component (it has ~D)."
            (length notes)))
    (when notes
      (filter-away-strings (xqdm:children (first notes))))))

(defmethod get-model-annotation ((model xqdm:elem-node))
  (let ((notes (get-sbml-element-annotations model)))
    (when (> (length notes) 1)
      (warn "CL-SBML: model ~A has more than 1 \"annotation\" component (it has ~D)."
            (length notes)))
    (when notes
      (filter-away-strings (xqdm:children (first notes))))))


(defmethod get-model-function-definitions ((model xqdm:elem-node))
  (get-element-components-in-list  model "listOfFunctionDefinitions"))

(defmethod get-model-unit-definitions ((model xqdm:elem-node))
  (get-element-components-in-list  model "listOfUnitDefinitions"))

(defmethod get-model-compartments ((model xqdm:elem-node))
  (get-element-components-in-list  model "listOfCompartments"))

(defmethod get-model-species ((model xqdm:elem-node))
  (get-element-components-in-list  model "listOfSpecies"))

(defmethod get-model-parameters ((model xqdm:elem-node))
  (get-element-components-in-list  model "listOfParameters"))

(defmethod get-model-rules ((model xqdm:elem-node))
  (get-element-components-in-list  model "listOfRules"))

(defmethod get-model-reactions ((model xqdm:elem-node))
  (get-element-components-in-list  model "listOfReactions"))

(defmethod get-model-events ((model xqdm:elem-node))
  (get-element-components-in-list  model "listOfEvents"))


;;; Function Definition node accessors.

(defmethod get-function-definition-id ((fd xqdm:elem-node))
  (get-sbml-element-id fd))

(defmethod get-function-definition-name ((fd xqdm:elem-node))
  (get-sbml-element-name fd))

(defmethod get-function-definition-math ((fd xqdm:elem-node))
  (get-element-components-in-list fd "math"))


;;; unit-definitions --

(defmethod get-unit-definition-id ((fd xqdm:elem-node))
  (get-sbml-element-id fd))

(defmethod get-unit-definition-name ((fd xqdm:elem-node))
  (get-sbml-element-name fd))

(defmethod get-unit-definition-units ((fd xqdm:elem-node))
  (get-element-components-in-list fd "units"))


;;; Compratments accessors.

(defmethod get-compartment-id ((c xqdm:elem-node))
  (get-sbml-element-id c))

(defmethod get-compartment-name ((c xqdm:elem-node))
  (get-sbml-element-name c))

(defmethod get-compartment-spatial-dimensions ((c xqdm:elem-node))
  (get-attr-named c "spatialDimensions"))

(defmethod get-compartment-size ((c xqdm:elem-node))
  (get-attr-named c "size"))

(defmethod get-compartment-units ((c xqdm:elem-node))
  (get-attr-named c "units"))

(defmethod get-compartment-outside ((c xqdm:elem-node))
  (get-attr-named c "outside"))

(defmethod get-compartment-constant ((c xqdm:elem-node))
  (get-attr-named c "constant"))


;;; Species accessors.

(defmethod get-species-id ((s xqdm:elem-node))
  (get-sbml-element-id s))

(defmethod get-species-name ((s xqdm:elem-node))
  (get-sbml-element-name s))

(defmethod get-species-compartment ((c xqdm:elem-node))
  (get-attr-named c "compartment"))

(defmethod get-species-boundary-condition ((c xqdm:elem-node))
  (get-attr-named c "boundaryCondition"))

(defmethod get-species-charge ((c xqdm:elem-node))
  (get-attr-named c "charge"))

(defmethod get-species-constant ((c xqdm:elem-node))
  (get-attr-named c "constant"))


;;; Species Reference accessors.

(defmethod get-species-reference-id ((s xqdm:elem-node))
  (get-sbml-element-id s))

(defmethod get-species-reference-name ((s xqdm:elem-node))
  (get-sbml-element-name s))

(defmethod get-species-reference-stoichiometry ((c xqdm:elem-node))
  (get-attr-named c "stoichiometry"))

(defmethod get-species-reference-denominator ((c xqdm:elem-node))
  (get-attr-named c "denominator"))

(defmethod get-species-reference-stoichiometry-math ((c xqdm:elem-node))
  (get-attr-named c "stoichiometryMath"))


;;; Modifier Species Reference accessors.

(defmethod get-modifier-species-reference-id ((s xqdm:elem-node))
  (get-sbml-element-id s))

(defmethod get-modifier-species-reference-name ((s xqdm:elem-node))
  (get-sbml-element-name s))


;;; Parameter accessors.

(defmethod get-parameter-id ((s xqdm:elem-node))
  (get-sbml-element-id s))

(defmethod get-parameter-name ((s xqdm:elem-node))
  (get-sbml-element-name s))

(defmethod get-parameter-value ((c xqdm:elem-node))
  (get-attr-named c "value"))

(defmethod get-parameter-units ((c xqdm:elem-node))
  (get-attr-named c "units"))

(defmethod get-parameter-constant ((c xqdm:elem-node))
  (get-attr-named c "constant"))


;;; Rule accessors.

(defmethod get-rule-id ((s xqdm:elem-node))
  (get-sbml-element-id s))

(defmethod get-rule-name ((s xqdm:elem-node))
  (get-sbml-element-name s))

(defmethod get-parameter-formula ((c xqdm:elem-node))
  (get-attr-named c "formula"))

(defmethod get-parameter-math ((c xqdm:elem-node))
  (get-attr-named c "math"))


;;; Algebraic Rule accessors.

(defmethod get-algebraic-rule-id ((s xqdm:elem-node))
  (get-sbml-element-id s))

(defmethod get-algebraic-rule-name ((s xqdm:elem-node))
  (get-sbml-element-name s))


;;; Assignment Rule accessors.

(defmethod get-assignment-rule-id ((s xqdm:elem-node))
  (get-sbml-element-id s))

(defmethod get-assignment-rule-name ((s xqdm:elem-node))
  (get-sbml-element-name s))

(defmethod get-assignment-rule-variable ((c xqdm:elem-node))
  (get-attr-named c "variable"))


;;; Rate Rule accessors.

(defmethod get-rate-rule-id ((s xqdm:elem-node))
  (get-sbml-element-id s))

(defmethod get-rate-rule-name ((s xqdm:elem-node))
  (get-sbml-element-name s))

(defmethod get-rate-rule-variable ((c xqdm:elem-node))
  (get-attr-named c "variable"))


;;; Reaction accessors.

(defmethod get-reaction-id ((s xqdm:elem-node))
  (get-sbml-element-id s))

(defmethod get-reaction-name ((c xqdm:elem-node))
  (get-sbml-element-name c))

(defmethod get-reaction-reactant-list ((c xqdm:elem-node))
  (get-element-components-in-list c "listOfReactants"))

(defmethod get-reaction-product-list ((c xqdm:elem-node))
  (get-element-components-in-list c "listOfProducts"))

(defmethod get-reaction-modifier-list ((c xqdm:elem-node))
  (get-element-components-in-list c "listOfModifiers"))

(defmethod get-reaction-kinetic-law ((c xqdm:elem-node))
  (get-element-component c "kineticLaw"))

(defmethod get-reaction-reversible ((c xqdm:elem-node))
  (get-attr-named c "reversible"))

(defmethod get-reaction-fast ((c xqdm:elem-node))
  (get-attr-named c "fast"))

;; Kinetic Law accessors.

(defmethod get-kinetic-law-formula ((c xqdm:elem-node))
  (get-attr-named c "formula"))

(defmethod get-kinetic-law-parameters ((c xqdm:elem-node))
  (get-element-components-in-list c "listOfParameters"))

(defmethod get-kinetic-law-math ((c xqdm:elem-node))
  (get-element-component c "math"))


;;; Events accessors.

(defmethod get-event-id ((s xqdm:elem-node))
  (get-sbml-element-id s))

(defmethod get-event-name ((s xqdm:elem-node))
  (get-sbml-element-name s))

(defmethod get-event-trigger ((c xqdm:elem-node))
  (get-attr-named c "trigger"))

(defmethod get-event-delay ((c xqdm:elem-node))
  (get-attr-named c "delay"))

(defmethod get-event-time-units ((c xqdm:elem-node))
  (get-attr-named c "timeUnits"))

(defmethod get-event-event-assignments ((c xqdm:elem-node))
  (get-attr-named c "eventAssignments"))


;;; end of file -- sbml-loader.lisp --
