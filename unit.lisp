;;; -*- Mode: Lisp -*-

(in-package "CL-SBML")

(defstruct (unit (:include sbml-base))
  "The SBML Unit Structure Class.
Instances of this structure class represent a (possibly transformed)
reference to a base unit chosen from UNIT-KIND."
  (kind :invalid :type unit-kind)
  (exponent 0 :type fixnum)
  (scale 1 :type fixnum)
  (multiplier 1.0d0 :type double)
  (offset 0.0d0 :type double)
  )

;;; end of file -- unit.lisp --
