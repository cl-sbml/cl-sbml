;;; -*- Mode: Lisp -*-

;;; cl-sbml.asd

;;;  *_cl-xml-load-file* --
;;; Change this variable to accomodate the location of CL-XML.
;;; If you already have CL-XML loaded in your image, this is optional.

(defparameter *_cl-xml-load-file*
  (merge-pathnames (make-pathname :directory '(:relative "lang" "cl" "cl-xml" "code")
                                  :name "load"
                                  :type "lisp")
                   (user-homedir-pathname)))


(eval-when (:load-toplevel :execute)
  (unless (find-package "XML-PARSER")
    (loop while (not (and *_cl-xml-load-file* (probe-file *_cl-xml-load-file*))) do
          (cerror "Enter the pathanme of CL-XML load file. Use either a string or a pathname."
                  "Cannot find the ~S file.~@
                   CL-XML is not available and therefore the system cannot~@
                   be setup."
                  *_cl-xml-load-file*)
          (format t "> ")
          (setf *_cl-xml-load-file*
                (ignore-errors (pathname (read))))
          (fresh-line)
          )
    (load *_cl-xml-load-file*)))


;;;; ADD HERE ASDF DIRECTORIES INFO.


;;; CL-SBML asdf system definition.

(asdf:defsystem "CL-SBML"
  :components ((:module "base"
                :components ("cl-sbml-package"
                             "sid"
                             "sbml-base"
                             "sbml-element"
                             "model"
                             "sbml"
                             "unit"
                             "unit-definition"
                             "unit-kind"
                             "compartment"
                             "event"
                             "event-assignment"
                             "function-definition"
                             "kinetic-law"
                             "species"
                             "parameter"
                             "reaction"
                             "stoichiometry-math"
                             "simple-species-reference"
                             (:file "modifier-species-reference"
                              :depends-on ("simple-species-reference"))
                             (:file "species-reference"
                              :depends-on ("simple-species-reference"))


                             "rule"
                             "algebraic-rule"
                             "rate-rule"
                             "assignment-rule"
               
               
                             "sbml-loader"
                             "build-model"
                             "sbml-writer"
                             ))
               #| #+lispworks
               (:module "gui"
                :components ("cl-sbml-interface")
                :depends-on ("base"))
	       |#
               )
  :depends-on ("cl-xml-ext" "cl-mathml"))


(eval-when (:load-toplevel :execute)
  (format t "~&;;; Setting the CL-SBML LP Translation.~%")
  (setf (logical-pathname-translations "CL-SBML")
        `(("*.*" ,(make-pathname :name :wild
                                 :type :wild
                                 :defaults *load-pathname*))
          ("**;*.*" ,(make-pathname :name :wild
                                    :type :wild
                                    :directory (append (or (pathname-directory *load-pathname*)
                                                           (list :relative) ; This :RELATIVE may not be right.
                                                           )
                                                       (list :wild-inferiors))
                                    :defaults *load-pathname*))
          )))



;;; end of file -- cl-sbml.asd --
