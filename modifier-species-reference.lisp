(in-package "CL-SBML")

(defstruct (modifier-species-reference (:include simple-species-reference))
  "The SBML Modifier Species Reference Structure Class."
  )

;;; end of file -- modifier-species-reference.lisp --
