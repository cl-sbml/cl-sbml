;;; -*- Mode: Lisp -*-

(in-package "CL-SBML")

(defun sid-p (x)
  "Tests whether the argument is a valid SBML \"SId\"."
  (and (stringp x)
       (or (alpha-char-p (char x 0))
           (char= #\_ (char x 0)))
       (every (lambda (c)
                (or (alphanumericp c)
                    (char= c #\_)))
              x)))


(deftype sid ()
  "The SBML \"SId\" type.
A string comprising only alphanumeric characters and underscores
starting with either a letter or an underscore."
  '(satisfies sid-p))


;;; end of file -- sid.lisp --
