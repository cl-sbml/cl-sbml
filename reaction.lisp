(in-package "CL-SBML")

(defstruct (reaction (:include sbml-element))
  "The SBML Reaction Structure Class.
A REACTION represents any transformation, transport or binding
process, typically a chemical reaction, that can change the amount of
one or more species."
  (reactants () :type list)
  (products () :type list)
  (modifiers () :type list)
  (kinetic-law nil :type (or null kinetic-law))
  (reversible-p nil :type boolean)
  (fast-p nil :type boolean)
  )

;;; end of file -- reaction.lisp --
