;;; -*- Mode: Lisp -*-

(in-package "CL-SBML")


;;; Protocol.

(defgeneric sbml-writer (sbml-item stream &key &allow-other-keys)
  (:documentation
   "The main function used to dump any SBML item to a STREAM.
Most of the time it will be called with a top level CL-SBML:MODEL to
produce an SBML output."))

(defgeneric write-element-notes (sbml-item stream))

(defgeneric write-element-annotation (sbml-item stream))


;;;===========================================================================
;;; Implementation.


;;;---------------------------------------------------------------------------
;;; Variables and constants.

(defvar *mathml-url* "http://www.w3.org/1998/Math.MathML")


;;;---------------------------------------------------------------------------
;;; Base methods and utility functions.


(defmethod sbml-writer ((x (eql 'nil)) (s stream) &key &allow-other-keys)
  ;; A no-op.
  )


(defmethod sbml-writer ((x list) (s stream)
                        &key
                        (list-name "listOfStuff" list-name-supplied-p)
                        &allow-other-keys)
  (when list-name-supplied-p (format s "~&<~A>~%" list-name))
  (pprint-logical-block (s x)
    (pprint-indent :block 4)
    (dolist (y x)
      (sbml-writer y s)))
  (when list-name-supplied-p (format s "~&</~A>~%" list-name)))


(declaim (inline element-has-notes-and-annotations-p))

(defun element-has-notes-or-annotations-p (e)
  (declare (type sbml-base e))
  (or (sbml-base-notes e) (sbml-base-annotation e)))


(defmethod write-element-notes ((sb sbml-element) (s stream))
  (let ((notes (sbml-base-notes sb)))
    (when notes
      (dolist (n notes t)
        (format s "~&<notes>~%")
        (sbml-writer n s)
        (format s "~&</notes>")))))


(defmethod write-element-annotation ((sb sbml-element) (s stream))
  (let ((annotation (sbml-base-annotation sb)))
    (when annotation
      (dolist (a annotation t)
        (format s "~&<annotation>~%")
        (sbml-writer a s)
        (format s "~&</annotation>")))))


;;;---------------------------------------------------------------------------
;;; model

(defmethod sbml-writer ((m model) (s stream) &key &allow-other-keys)
  (format s "~&<model name=~S id=~S>~%"
          (sbml-element-name m)
          (sbml-element-id m))

  #|
  (sbml-writer (model-notes m) s)
  (sbml-writer (model-annotation m) s)
  |#

  (write-element-notes m s)
  (write-element-annotation m s)


  (sbml-writer (model-compartments m) s :list-name "listOfCompartments")

  (sbml-writer (model-species m) s  :list-name "listOfSpecies")

  (sbml-writer (model-reactions m) s  :list-name "listOfReactions")

  (format s "</model>~%"))


(defmethod sbml-writer :around ((m model) (s stream)
                                &key
                                (full-document t)
                                (sbml-level 2)
                                (sbml-version 1)
                                &allow-other-keys)
  (cond (full-document
         (format s "<?xml version=\"1.0\" encoding=\"UTF-8\"?>")
         (format s "<sbml xmlns=\"http://www.sbml.org/sbml/level~D\" ~
                    level=\"~D\" version=\"~D\">"
                 sbml-level
                 sbml-level
                 sbml-version)
         (call-next-method)
         (format s "</sbml>")
         )
        (t
         (call-next-method))))


;;;---------------------------------------------------------------------------
;;; compartment

(defmethod sbml-writer ((c compartment) (s stream) &key &allow-other-keys)
  (format s (formatter "~&<compartment~@[ name=~S~]~@[ id=~S~]~@[ spatialDimensions=~D~]~@[ size=~S~]")
          (sbml-element-name c)
          (sbml-element-id c)
          (compartment-spatial-dimensions c)
          (compartment-size c)
          #|...|#
          )

      
  (cond ((element-has-notes-or-annotations-p c)
         (format s ">~%")
         (write-element-notes c s)
         (write-element-annotation c s)
         (format s "</comparment>~%"))
        (t
         (format s "/>~%"))))


;;;---------------------------------------------------------------------------
;;; species

(defmethod sbml-writer ((e species) (s stream) &key &allow-other-keys)
  (format s (formatter "~&<species~@[ name=~S~]~@[ id=~S~]~@[ compartment=~S~]~@[ charge=~D~]")
          (sbml-element-name e)
          (sbml-element-id e)
          (species-compartment e)
          (species-charge e)
          #|...|#
          )
      
  (cond ((element-has-notes-or-annotations-p e)
         (format s ">~%")
         (write-element-notes e s)
         (write-element-annotation e s)
         (format s "</species>~%"))
        (t
         (format s "/>~%"))))


;;;---------------------------------------------------------------------------
;;; reactions

(defmethod sbml-writer ((e reaction) (s stream) &key &allow-other-keys)
  (format s (formatter "~&<reaction~@[ name=~S~]~
                        ~:[ fast=\"false\"~; fast=\"false\"~]~
                        ~:[ reversible=\"false\"~; reversible=\"false\"~]~
                        >")
          (reaction-name e)
          ;; (sbml-element-id e)
          (reaction-fast-p e)
          (reaction-reversible-p e)
          (reaction-kinetic-law e)
          #|...|#
          )
      
  (when (element-has-notes-or-annotations-p e)
    (write-element-notes e s)
    (write-element-annotation e s))

  (sbml-writer (reaction-reactants e) s :list-name "listOfReactants")
  (sbml-writer (reaction-products e) s :list-name "listOfProducts")

  (sbml-writer (reaction-kinetic-law e) s)
  (format s "~&</reaction>~%"))
    


;;;---------------------------------------------------------------------------
;;; kinetic-law

(defmethod sbml-writer ((e kinetic-law) (s stream) &key &allow-other-keys)
  (format s (formatter "~&<kineticLaw~@[ formula=~S~]")
          (kinetic-law-formula e)
          #|...|#
          )
  
  (cond ((or (element-has-notes-or-annotations-p e)
             (kinetic-law-math e)
             (kinetic-law-parameters e))

         (format s ">~%")
         (when (kinetic-law-math e)
           (write-element-notes e s)
           (write-element-annotation e s))

         (when (kinetic-law-math e)
           (format s "~&<math xmlns=~S>~%" *mathml-url*)
           (sbml-writer (kinetic-law-math e) s)
           (format s "~&</math>~%"))

         (when (kinetic-law-parameters e)
           (sbml-writer (kinetic-law-parameters e) s :list-name "listOfParameters"))

         (format s "~&</kineticLaw>~%"))
        (t
         (format s ">~%"))
        ))


;;;---------------------------------------------------------------------------
;;; simple-species-reference, species-reference

(defmethod sbml-writer ((e simple-species-reference) (s stream) &key &allow-other-keys)
  (format s (formatter "~&<simpleSpeciesReference~@[ species=~A~]")
          (simple-species-reference-species e)
          #|...|#
          )
  
  (cond ((element-has-notes-or-annotations-p e)
         (format s ">~%")
         (write-element-notes e s)
         (write-element-annotation e s)
         (format s "~&</simpleSpeciesReference>~%"))
        (t
         (format s "/>~%"))
        ))


(defmethod sbml-writer ((e species-reference) (s stream) &key &allow-other-keys)
  (format s (formatter "~&<speciesReference~@[ species=~S~] ~
                        denominator=~D")
          (species-reference-species e)
          (species-reference-denominator e)
          #|...|#
          )
  
  (cond ((element-has-notes-or-annotations-p e)
         (format s ">~%")
         (write-element-notes e s)
         (write-element-annotation e s)
         (format s "~&</speciesReference>~%"))
        (t
         (format s "/>~%"))
        ))


;;;---------------------------------------------------------------------------
;;; parameter

(defmethod sbml-writer ((e parameter) (s stream) &key &allow-other-keys)
  (format s (formatter "~&<parameter~@[ name=~S~]~@[ id=~S~] ~
                        value=~S ~
                        constant=~:[\"false\"~;\"true\"~]")
          (sbml-element-name e)
          (sbml-element-id e)

          (parameter-value e)
          (parameter-constant-p e)
          #|...|#
          )
      
  (cond ((element-has-notes-or-annotations-p e)
         (format s ">~%")
         (write-element-notes e s)
         (write-element-annotation e s)
         (format s "</parameter>~%"))
        (t
         (format s "/>~%"))
        ))


;;;---------------------------------------------------------------------------
;;; Catch all methods.

(defmethod sbml-writer ((e string) (s stream) &key &allow-other-keys)
  (format s "~A" e))

(defmethod sbml-writer ((e xqdm:elem-node) (s stream) &key &allow-other-keys)
  (fresh-line s)
  (xqdm:write-node e s))

(defmethod sbml-writer ((e sbml-base) (s stream) &key &allow-other-keys)
  (warn "CL-SBML: SBML-WRITER method for ~A not yet implemented."
        (type-of e))
  (format s "~&~A" e))

;;; end of file -- sbml-writer.lisp --
