(in-package "CL-SBML")


(defstruct (sbml-element (:include sbml-base))
  "The SBML Element Structure Class.
Most SBML elements share a ID and a NAME field.
This structure class provides them both and it is used as a mixin in
all the `element' definitions."

  (id nil :type (or symbol string sid))
  (name "" :type string)
  )

;;; end of file -- sbml-base.lisp --
