(in-package "CL-SBML")

(defstruct (stoichiometry-math (:include sbml-base))
  "The SBML Stoichiometry Math Structure Class."
  (math () :type (or null cons t)) ; To accommodate XQDM:DOC-NODE.
  (parsed-math () :type (or null cons))
  )

;;; end of file -- stoichiometry-math.lisp --
