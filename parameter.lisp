(in-package "CL-SBML")

(defstruct (parameter (:include sbml-element))
  "The Parameter Structure Class.
The instances of this structure class represent SBML parameter
definitions."
  (value 0.0d0 :type double-float)
  (units nil :type (or symbol string cons)) ; Actually, the STRING type should be a SID.
  (constant-p nil :type boolean)
  )

;;; end of file -- parameter.lisp --
