(in-package "CL-SBML")

(defstruct (assignment-rule (:include rule))
  "The SBML Assignment Rule Structure CLass."
  (variable nil :type (or symbol string))
  )

;;; end of file -- assignment-rule.lisp --
