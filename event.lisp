(in-package "CL-SBML")

(defstruct (event (:include sbml-element))
  "The SBML Event Structure Class."
  (trigger () :type (or null cons t)) ; T to accommodate a MatML XQDM:NODE
  (delay () :type (or null cons t) ; T to accommodate a MatML XQDM:NODE
  (time-units nil :type (or symbol string)) ; Actually a SID.
  (event-assignments () :type list)
  )

;;; end of file -- function-definition.lisp --
