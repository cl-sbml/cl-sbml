(in-package "CL-SBML")

(defstruct (rule (:include sbml-element) (:constructor nil))
  "The SBML Rule Structure Class.
Rules provide a way to create constraints on variables for cases in
which the constraints cannot be expressed using reactions nor the
assignment of an initial value to a component in a model.

The Rule class is used as a mixin for more specialized classes.  Hence
there is no constructor available for it."
  (formula "" :type string)
  (math () :type cons)
  )

;;; end of file -- rule.lisp --
